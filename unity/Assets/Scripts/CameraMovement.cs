﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour {

	private Transform passaro;
	private Transform InfoSuperior;

	// Use this for initialization
	void Start () {
		passaro = GameObject.Find("passaro").transform;
		InfoSuperior = GameObject.Find("InfoSuperior").transform;
	}
	
	// Update is called once per frame
	void Update () {
		transform.position = new Vector3 (passaro.position.x+6.79f, transform.position.y, transform.position.z);
		InfoSuperior.position = new Vector3 (passaro.position.x+6.79f-9f, InfoSuperior.position.y, InfoSuperior.position.z);
	}
}

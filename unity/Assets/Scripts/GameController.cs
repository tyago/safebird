﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour {

	private int hearts = 5;
	private int fasesTotal = 3;
	private int fasesFinalizadas = 0;

	// Use this for initialization
	void Start () {
		DontDestroyOnLoad (gameObject);
	}
	
	// Update is called once per frame
	void Update () {
		Scene scene = SceneManager.GetActiveScene();
		if( "Menu".Equals(scene.name) ){
			GameObject[] rootGameObjects = scene.GetRootGameObjects();
			for (int i = 0; i < rootGameObjects.Length; i++){
				if( rootGameObjects[i] != null && rootGameObjects[i].name != null && rootGameObjects[i].name.StartsWith("menufase") ){
					int mfId = int.Parse( rootGameObjects[i].name.Substring(8) );
					SpriteRenderer sprite = rootGameObjects[i].GetComponent<SpriteRenderer>();
					if( mfId <= fasesFinalizadas+1 ){
						sprite.color = Color.white;
					}
				}
			}

		}
	}

	public void morreu () {
		hearts--;
		if (hearts > 0){
			SceneManager.LoadScene("Menu");
		}
		else{
			SceneManager.LoadScene("GameOver");
		}
	}

	public void faseFinalizada(int f){
		if( f > fasesFinalizadas  ){
			fasesFinalizadas = f;
		}

		if( isVitoria() ){
			Debug.Log ("Venceu o jogo!!!");
			SceneManager.LoadScene("Vitoria");
		}
		else{
			SceneManager.LoadScene("Menu");
		}
	}

	public bool isVitoria(){
		return fasesFinalizadas == fasesTotal;
	}

	public int getHearts(){
		return hearts;
	}
}

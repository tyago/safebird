﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlvoController : MonoBehaviour {

	private bool calculado;
	private float tempo;
	private float atraso;
	private float delta;
	private float novoY;

	private Transform passaro;

	// Use this for initialization
	void Start () {
		passaro = GameObject.Find("passaro").transform;
	}
	
	// Update is called once per frame
	void Update () {
		if( Mathf.Abs( passaro.position.x - transform.position.x ) <= 20f ){
			if( calculado ){
				if( tempo + atraso > Time.time ){
					transform.position = new Vector3 (transform.position.x, novoY , transform.position.z);
					calculado = false;
				}
			}
			else
			{
				atraso = Random.Range(8f, 10f);
				delta = Random.Range(0.01f, 0.05f);

				novoY = transform.position.y;
				if( passaro.position.y > transform.position.y ){
					novoY += delta;
				}
				else if( passaro.position.y < transform.position.y ){
					novoY -= delta;
				}

				calculado = true;
				tempo = Time.time;
			}
		}
	}

}

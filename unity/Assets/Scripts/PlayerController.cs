﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour {

	private Rigidbody2D rb;
	private GameObject gameObjController;
	private GameController gameController;

	private string nomeFase;
	private int faseId;
	private int hearts;

	private bool acelerar = false;
	public float velocH = 2f;
	public float acelerAdicionalH = 4f;

	private bool subir = false;
	public float forcaSubidaV = 2f;

	// Use this for initialization
	void Start() {
		rb = GetComponent<Rigidbody2D> ();
		gameObjController = GameObject.FindGameObjectWithTag ("GameController");
		gameController = gameObjController.GetComponent<GameController>();

		nomeFase = gameObject.scene.name;
		if( nomeFase.StartsWith("Fase") ){
			faseId = int.Parse( nomeFase.Substring(4) );
		}
		hearts = gameController.getHearts();

		for (int h = hearts+1; h <= 5; h++){
			Transform heartObj = GameObject.Find("heart"+h).transform;
			heartObj.position = new Vector3 (heartObj.position.x, heartObj.position.y, -30);
		}

		//Debug.Log("nomeFase = " + nomeFase + " / faseId = " + faseId + " / hearts = " + hearts);
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetButtonDown ("Jump") ) {
			subir = true;
		}
		if (Input.GetButtonDown ("Horizontal") ) {
			acelerar = true;
		}
	}

	void FixedUpdate() {
		rb.velocity = new Vector2(velocH, rb.velocity.y);
		//rb.AddForce( new Vector2(velocH, 0f) );
		if (acelerar) {
			rb.AddForce( new Vector2(acelerAdicionalH, 0f) );
			acelerar = false;
		}
		if (subir) {
			rb.AddForce( new Vector2(0f, forcaSubidaV) );
			subir = false;
		}
	}

	void OnCollisionEnter2D (Collision2D col) {
		if( "Fim".Equals(col.gameObject.name) ){
			Debug.Log ("Venceu a fase!!!");
			gameController.faseFinalizada(faseId);
		} 
		else if (col.gameObject.CompareTag ("Morte")) {
			Debug.Log ("MORREU");
			gameController.morreu();
		}
		
	}

}

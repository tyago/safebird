using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class MenuFase : MonoBehaviour {

	private int faseId;

	// Use this for initialization
	void Start () {
		faseId = int.Parse( gameObject.name.Substring(8) );
		//Debug.Log ("Iniciado " + gameObject.name);
		//DontDestroyOnLoad (gameObject);
	}
	
	// Update is called once per frame
	void Update () {
	}

	void OnMouseDown()
    {
		SpriteRenderer sprite = GetComponent<SpriteRenderer>();
		if( faseId == 1 || sprite.color == Color.white ){
			Debug.Log ("Iniciar fase " + faseId);
			SceneManager.LoadScene("Fase"+faseId);
		}
    }

}
